import java.sql.*;

public class Offering {
	private final int id;
	private final Course course;
	private final String daysTimes;

	public static Offering create(Course course, String daysTimesCsv) throws Exception {
		return DBSupport.createOffering(course, daysTimesCsv);
	}

	public static Offering find(int id) throws SQLException {
		return DBSupport.findOffering(id);
	}

	public void update() throws Exception {
		DBSupport.updateOffering(this);
	}

	public Offering(int id, Course course, String daysTimesCsv) {
		this.id = id;
		this.course = course;
		this.daysTimes = daysTimesCsv;
	}

	public int getId() {
		return id;
	}

	public Course getCourse() {
		return course;
	}

	public String getDaysTimes() {
		return daysTimes;
	}

	public String toString() {
		return "Offering " + getId() + ": " + getCourse() + " meeting " + getDaysTimes();
	}
}
