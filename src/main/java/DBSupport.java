import javax.swing.plaf.nimbus.State;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DBSupport {
    private static final String DBNAME = "reggie";
    private static final String connectionURL = "jdbc:derby:" + DBNAME + ";create=true";

    private static Connection connection = null;

    public static Connection getConnection() throws SQLException {
        if (connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection(connectionURL);
        }
        return connection;
    }

    public static void closeConnection(){
        try {
            connection.close();
        } catch (Exception ignored) {}
    }

    static class SQLExecutor{
        private final Statement statement;

        public SQLExecutor() throws SQLException {
            this.statement = getConnection().createStatement();
        }

        public int executeUpdate(String sql) throws SQLException {
            return this.statement.executeUpdate(sql);
        }

        public ResultSet executeQuery(String sql) throws SQLException{
            return this.statement.executeQuery(sql);
        }

        public void close() throws SQLException {
            this.statement.close();
            closeConnection();
        }
    }


    public static void deleteAll(String dbName) throws Exception {
        SQLExecutor executor = new SQLExecutor();
        try {
            executor.executeUpdate("DELETE FROM " + dbName);

        } finally {
            executor.close();
        }
    }

    public static Course createCourse(String name, int credits) throws Exception {
        SQLExecutor executor = new SQLExecutor();
        try {
            executor.executeUpdate("DELETE FROM course WHERE name = '" + name + "'");
            executor.executeUpdate("INSERT INTO course VALUES ('" + name + "', " + credits + ")");

            return new Course(name, credits);
        } finally {
            executor.close();
        }
    }

    public static Schedule createSchedule(String name) throws Exception {
        SQLExecutor executor = new SQLExecutor();
        try {
            executor.executeUpdate("DELETE FROM schedule WHERE name = '" + name + "'");

            return new Schedule(name);
        } finally {
            executor.close();
        }
    }

    public static Offering createOffering(Course course, String daysTimesCsv) throws Exception{
        SQLExecutor executor = new SQLExecutor();
        try {
            ResultSet result = executor.executeQuery("SELECT MAX(id) FROM offering");
            result.next();
            int newId = 1 + result.getInt(1);

            executor.executeUpdate("INSERT INTO offering VALUES (" + newId + ",'"
                    + course.getName() + "','" + daysTimesCsv + "')");

            result.close();
            return new Offering(newId, course, daysTimesCsv);
        } finally {
            executor.close();
        }
    }

    public static Schedule findSchedule(Schedule schedule) throws SQLException {
        SQLExecutor executor = new SQLExecutor();
        try {
            ResultSet result = executor.executeQuery(
                    "SELECT * FROM schedule WHERE name= '" + schedule.getName() + "'");
            getConnection().setAutoCommit(false);
            while (result.next()) {
                Offering offering = findOffering(result.getInt("offeringId"));
                if (offering != null) {
                    schedule.add(offering);
                }
            }

            result.close();
            return schedule;
        } finally {
            executor.close();
        }
    }

    public static Course findCourse(String name) throws SQLException {
        SQLExecutor executor = new SQLExecutor();
        try {
            ResultSet result = executor.executeQuery("SELECT * FROM course WHERE name = '" + name
                    + "'");
            if (!result.next())
                return null;
            int credits = result.getInt("Credits");

            result.close();
            return new Course(name, credits);
        } finally {
            executor.close();
        }
    }

    public static Offering findOffering(int id) throws SQLException{
        SQLExecutor executor = new SQLExecutor();
        try {
            ResultSet result = executor.executeQuery("SELECT * FROM offering WHERE id =" + id
                    + "");
            if (!result.next())
                return null;

            Offering offering = new Offering(id,
                    findCourse(result.getString("name")),
                    result.getString("daysTimes"));
            result.close();
            return offering;
        } catch (Exception ex) {
            executor.close();
            return null;
        }
    }

    public static void updateCourse(String name, int credits) throws Exception {
        SQLExecutor executor = new SQLExecutor();
        try {
            executor.executeUpdate("DELETE FROM course WHERE name = '" + name + "'");
            executor.executeUpdate("INSERT INTO course VALUES('" + name + "'," + credits + ")");

        } finally {
            executor.close();
        }
    }

    public static void updateSchedule(Schedule schedule) throws Exception{
        SQLExecutor executor = new SQLExecutor();
        try {
            executor.executeUpdate("DELETE FROM schedule WHERE name = '" + schedule.getName() + "'");
            for (int i = 0; i < schedule.getSize(); i++) {
                executor.executeUpdate(
                        "INSERT INTO schedule VALUES('" + schedule.getName() + "'," +
                                schedule.getOffering(i).getId() + ")");
            }

        } finally {
            executor.close();
        }
    }

    public static void updateOffering(Offering offering) throws Exception{
        SQLExecutor executor = new SQLExecutor();
        try {
            executor.executeUpdate("DELETE FROM offering WHERE id=" + offering.getId() + "");
            executor.executeUpdate("INSERT INTO offering VALUES(" + offering.getId() + ",'" +
                    offering.getCourse().getName() + "','" + offering.getDaysTimes() + "')");
        } finally {
            executor.close();
        }
    }

    public static ArrayList<Schedule> allSchedules() throws Exception{
        SQLExecutor executor = new SQLExecutor();
        try {
            ResultSet results = executor.executeQuery("SELECT DISTINCT name FROM schedule ORDER BY name");

            ArrayList<Schedule> result = new ArrayList<>();
            while (results.next())
                result.add(Schedule.find(results.getString("name")));

            results.close();
            return result;
        } finally {
            executor.close();
        }
    }
}
