import java.sql.*;

public class Course {
	private final String name;
	private final int credits;

	Course(String name, int credits) {
		this.name = name;
		this.credits = credits;
	}

	public int getCredits() {
		return credits;
	}

	public String getName() {
		return name;
	}

	public static Course create(String name, int credits) throws Exception {
		return DBSupport.createCourse(name, credits);
	}

	public static Course find(String name) throws SQLException {
		return DBSupport.findCourse(name);
	}

	public void update() throws Exception {
		DBSupport.updateCourse(name, credits);
	}
}
