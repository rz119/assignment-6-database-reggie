import java.util.*;
import java.sql.*;

public class Schedule {
	private final String name;
	int credits = 0;
	static final int minCredits = 12;
	static final int maxCredits = 18;
	boolean overloadAuthorized = false;
	ArrayList<Offering> schedule = new ArrayList<>();

	public String getName(){
		return name;
	}

	public int getSize(){
		return schedule.size();
	}

	public Offering getOffering(int i){
		if (i >= 0 && i < schedule.size()){
			return schedule.get(i);
		}
		return null;
	}

	public static void deleteAll() throws Exception {
		DBSupport.deleteAll("schedule");
	}

	public static Schedule create(String name) throws Exception {
		return DBSupport.createSchedule(name);
	}

	public static Schedule find(String name) throws SQLException {
		Schedule schedule = new Schedule(name);
		return DBSupport.findSchedule(schedule);
	}

	public static Collection<Schedule> all() throws Exception {
		return DBSupport.allSchedules();
	}

	public void update() throws Exception {
		DBSupport.updateSchedule(this);
	}

	public Schedule(String name) {
		this.name = name;
	}

	public void add(Offering offering) {
		credits += offering.getCourse().getCredits();
		schedule.add(offering);
	}

	public void authorizeOverload(boolean authorized) {
		overloadAuthorized = authorized;
	}

	public List<String> analysis() {
		ArrayList<String> result = new ArrayList<>();

		if (credits < minCredits)
			result.add("Too few credits");

		if (credits > maxCredits && !overloadAuthorized)
			result.add("Too many credits");

		checkDuplicateCourses(result);

		checkOverlap(result);

		return result;
	}

	public void checkDuplicateCourses(ArrayList<String> analysis) {
		HashSet<Course> courses = new HashSet<>();
		for (Offering offering : schedule) {
			Course course = offering.getCourse();
			if (courses.contains(course))
				analysis.add("Same course twice - " + course.getName());
			courses.add(course);
		}
	}

	public void checkOverlap(ArrayList<String> analysis) {
		HashSet<String> times = new HashSet<>();

		for (Offering offering : schedule) {
			String daysTimes = offering.getDaysTimes();
			StringTokenizer tokens = new StringTokenizer(daysTimes, ",");
			while (tokens.hasMoreTokens()) {
				String dayTime = tokens.nextToken();
				if (times.contains(dayTime))
					analysis.add("Course overlap - " + dayTime);
				times.add(dayTime);
			}
		}
	}

	public String toString() {
		return "Schedule " + name + ": " + schedule;
	}
}
